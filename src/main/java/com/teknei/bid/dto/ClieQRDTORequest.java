package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClieQRDTORequest implements Serializable {

    private Integer idClient;
    private String username;

}